<?php
$webTitle = "Sistem Pengurusan Lesen Anjing - Dewan Bandaraya Kuala Lumpur";
$webDesc = "Permohonan baru atau pembaharuan lesen anjing secara atas talian yang disediakan oleh Dewan Bandaraya Kuala Lumpur";
$webTitle_en = "Dog License Management System - Dewan Bandaraya Kuala Lumpur";
$webDesc_en = "New online application or renewal of dog license provided by Kuala Lumpur City Hall (Dewan Bandaraya Kuala Lumpur";
$webImg = "http://edog.dbkl.gov.my/img/edog-1.jpg";
$webURL = "http://edog.dbkl.gov.my/";
$formURL = "http://edog.dbkl.gov.my/index.php";
?>