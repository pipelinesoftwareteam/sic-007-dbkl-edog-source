<div class="div-lang">
	<a href="index.php">Bahasa Melayu</a> | <strong>English</strong>
</div>
<img src="img/logo_edog.png" width="40">
<nav>
                        <!-- Menu Toggle -->
                        <!-- Toggles menu on small screens -->
                        <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                            <i class="fa fa-bars"></i>
                        </a>
                        <!-- END Menu Toggle -->

                        <!-- Main Menu -->
                        <ul class="site-nav">
                            <!-- Toggles menu on small screens -->
                            <li class="visible-xs visible-sm">
                                <a href="javascript:void(0)" class="site-menu-toggle text-center">
                                    <i class="fa fa-times"></i>
                                </a>
                            </li>
                            <!-- END Menu Toggle -->
                            <li <?php if($thispage == "home"){ echo "class=\"active\"";} ?>>
                                <a href="index_en.php" >Home</a>

                            </li>
                            <li <?php if($thispage == "manual"){ echo "class=\"active\"";} ?>>
                                <a href="manual_en.php">User Manual</a>
                            </li>
                            <li <?php if($thispage == "faq"){ echo "class=\"active\"";} ?>>
                                <a href="faq_en.php">FAQ</a>
                            </li>
                            <li <?php if($thispage == "contact"){ echo "class=\"active\"";} ?>>
                                <a href="contact_us_en.php">Contact Us</a>
                            </li>
  
                            <li>
                                 <a href="<?php echo $formURL ?>/login" class="btn btn-primary">Log In</a> <!---->
                            </li>
                            <li>
                                <a href="<?php echo $formURL ?>/login#register" class="btn btn-success">Sign Up</a>
                            </li>
                        </ul>
                        <!-- END Main Menu -->
                    </nav>