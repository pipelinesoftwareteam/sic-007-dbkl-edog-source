<div class="div-lang">
	<strong>Bahasa Melayu</strong> | <a href="index_en.php">English</a>
</div>
<img src="img/logo_edog.png" width="40">
<nav>
                        <!-- Menu Toggle -->
                        <!-- Toggles menu on small screens -->
                        <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                            <i class="fa fa-bars"></i>
                        </a>
                        <!-- END Menu Toggle -->

                        <!-- Main Menu -->
                        <ul class="site-nav">
                            <!-- Toggles menu on small screens -->
                            <li <?php if($thispage == "home"){ echo "class=\"active\"";} ?>>
                                <a href="index.php" >Utama</a>

                            </li>
                            <li <?php if($thispage == "manual"){ echo "class=\"active\"";} ?>>
                                <a href="manual.php">Manual Pengguna</a>
                            </li>
                            <li <?php if($thispage == "faq"){ echo "class=\"active\"";} ?>>
                                <a href="faq.php">Soalan Lazim</a>
                            </li>
                            <li <?php if($thispage == "contact"){ echo "class=\"active\"";} ?>>
                                <a href="contact_us.php">Hubungi Kami</a>
                            </li>

                            <li>
                                <a href="<?php echo $formURL ?>/login" class="btn btn-primary">Log Masuk</a> <!---->
                            </li>
                            <li>
                                <a href="<?php echo $formURL ?>/login#register" class="btn btn-success">Daftar</a>
                            </li>
                        </ul>
                        <!-- END Main Menu -->
                    </nav>